
require "octokit"

module Ydashboard
  class GhIssuesQuery
    GITHUB_ORG = "yast"

    attr_reader :org

    def initialize(org = GITHUB_ORG)
      @org = org
    end

    def run
      # return { "yast" => { "yast-yast2" => Octokit.issues("yast/yast-yast2")}}

      # fetch all resources
      Octokit.auto_paginate = true
      result = {}

      client.repos(org).each do |repo|
        repo_name = repo[:name]
        issues = repo[:open_issues_count] > 0 ? client.issues("#{org}/#{repo_name}") : []
        result[repo_name] = issues
      end

      { org => result }
    end

  private

    def client
      return @client if @client

      client_args =
        if !ENV.fetch("YDASHBOARD_GH_ACCESS_TOKEN", "").empty?
          { access_token: ENV["YDASHBOARD_GH_ACCESS_TOKEN"] }
        else
          {}
        end
      @client = Octokit::Client.new(client_args)
    end
  end
end






