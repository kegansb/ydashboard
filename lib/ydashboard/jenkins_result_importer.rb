
require_relative "jenkins_log_analyzer"

module Ydashboard
  class JenkinsResultImporter
    attr_reader :jenkins_query

    def initialize(jenkins_query)
      @jenkins_query = jenkins_query
    end

    def import
      data = jenkins_query.run

      JenkinsStatus.transaction do
        JenkinsStatus.where(internal: jenkins_query.internal_jenkins).destroy_all

        data.each do |job|
          next if job["color"] == "disabled"
          job_stat = JenkinsStatus.create(name: job["name"],
            internal: jenkins_query.internal_jenkins,
            success: job["color"] == "blue")

          analyze_log(job_stat, job) if job["color"] == "red"
        end
      end
    end

    def analyze_log(job_stat, job)
      analyzer = JenkinsLogAnalyzer.new((job["console"] || {})["output"])
      failures, actions = analyzer.analyze

      failures_text = failures.join("\n")
      actions_text = actions.join("\n")

      job_stat.jenkins_build_failures.create(build: job["build"], failures: failures_text,
        actions: actions_text, author: analyzer.author)
    end
  end
end
