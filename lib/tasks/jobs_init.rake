namespace :jobs do
  # schedule the backround jobs to run them regularly
  desc 'Schedule the background jobs to run now'
  task init: :environment do
    Rails.logger.info "Scheduling the background jobs..."
    # clear the queue
    Delayed::Job.delete_all

    # enqueue all jobs
    JenkinsQueryJob.perform_later
    BugzillaQueryJob.perform_later
    ObsQueryJob.perform_later
    GhQueryJob.perform_later
    BugzillaClosedQueryJob.perform_later
  end

  # schedule the jobs when starting the worker
  task work: :init
end
