
namespace :download do
  # A task for downloading the initial data or explicitly refreshing the database
  desc 'Download data from the external sources (initialize DB)'
  task data: :environment do
    puts "Started downloading external data"

    puts "Downloading Jenkins data..."
    # FIXME: handle unavailable internal Jenkins better
    JenkinsQueryJob.new.perform rescue SocketError

    puts "Downloading open Bugzilla bugs..."
    BugzillaQueryJob.new.perform
    
    puts "Downloading closed Bugzilla bugs..."
    BugzillaClosedQueryJob.new.perform
    
    puts "Downloading GitHub data..."
    GhQueryJob.new.perform rescue Octokit::TooManyRequests

    puts "Downloading OBS data..."
    ObsQueryJob.new.perform
  end
end
