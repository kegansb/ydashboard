
# Use a different logger for distributed setups.
# require 'syslog/logger'
# config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

# log to STDOUT instead of the log/*.log files, used in Docker containers
if ENV["RAILS_LOG_TO_STDOUT"].present?
  Rails.application.configure do
    puts "Redirecting the log to STDOUT"
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger = ActiveSupport::TaggedLogging.new(logger)
    Rails.logger = config.logger
    ActiveRecord::Base.logger = config.logger
    ActionController::Base.logger = config.logger
  end
end
