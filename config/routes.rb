# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  get 'jenkins/index'

  get 'obs/index'

  get 'bugzilla/graphs'

  get 'bugzilla/summary'

  get 'bugzilla/index'

  root 'dashboard#index'

  get 'dashboard/index'
end
