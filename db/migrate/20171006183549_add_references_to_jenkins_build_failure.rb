class AddReferencesToJenkinsBuildFailure < ActiveRecord::Migration[5.0]
  def change
    add_reference :jenkins_build_failures, :jenkins_status, foreign_key: true
  end
end
