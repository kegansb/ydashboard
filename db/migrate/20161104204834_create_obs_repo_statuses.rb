class CreateObsRepoStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :obs_projects do |t|
      # project name, e.g. "YaST:Head"
      t.string :name
      # OBS/IBS flag
      t.boolean :internal, null: false

      t.timestamps
    end

    create_table :obs_repo_statuses do |t|
      t.belongs_to :obs_project, index: true
      # build target name, e.g. "SLES_12_SP1"
      t.string :name
      # number of failes (for all architectures)
      t.integer :failures, default: 0, null: false

      t.timestamps
    end

    add_index :obs_projects, :name

    add_index :obs_repo_statuses, :name
  end
end
