class CreateJenkinsStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :jenkins_statuses do |t|
      t.string :name, null: false
      t.boolean :internal, null: false
      t.boolean :success, null: false

      t.timestamps
    end
    add_index :jenkins_statuses, :name
    add_index :jenkins_statuses, :internal
  end
end
