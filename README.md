# YDashboard

The YDashboard Web Application is a proposal for the YaST team dashboard
page with the overview of the current status collected from many services.

You can see the [public instance running live](https://ydashboard.herokuapp.com).

![Screenshot](./ydashboard.png)

## Configuration

- Optionally copy the `config/bugzilla.yml.template` file to `config/bugzilla.yml` and write
your bugzilla.suse.com credentials there. Otherwise only the public visible bugs
will be found. (But that is actually correct for public deployment to avoid
leaking internal bugs.)

## Running the Server

### Initialization

It is recommended to use the Docker envirmonment (see below), it is much easier
for configuration and deployment.

- Install the needed Ruby gems: `bundle install --path vendor/bundle`
- Configure the database
  - Use a preconfigured Docker container (see below)
  - Install and configure Postgres locally
  - Switch to SQLite (replace `pg` Ruby gem with `sqlite3`, adapt the
    `config/database.yml` file)
- Initalize the database: `bundle exec rake db:create` and
  `bundle exec rake db:migrate`

### Run

To run the server (in development):

```shell
# clear the job queue
bundle exec rake jobs:clear
bundle exec bin/rails s
```

## Delayed Jobs

To run the background job worker run

```shell
bundle exec rake jobs:work
```

## Using Docker Compose

In order to make development easier, this repository includes a sample configuration file
to run *YDashboard* using [Docker Compose](https://docs.docker.com/compose/). To use it,
just follow these steps:

- Install Docker and Docker Compose (`zypper in docker docker-compose`)
- Start the Docker daemon (`systemctl start docker`), optionally enable at boot
  (`systemctl enable docker`)
- If you want to develop the YDashboard and want to avoid rebuilding the Docker
  image after every change then use the
  `-f docker-compose.yml -f docker-compose.development.yml` options for the
  `docker-compose` commands below. See the [multiple compose files documentation](
  https://docs.docker.com/compose/extends/#multiple-compose-files).

To start the project, just run:

```shell
# optionally export the `YDASHBOARD_GH_ACCESS_TOKEN` environment variable
# with a GitHub access token to avoid reaching the GitHub API rate limit
# TODO: avoid using env, it is not much secure...
docker-compose build
docker-compose run web bundle exec rake db:create
docker-compose run web bundle exec rake db:schema:load
# optionally initialize the Bugzilla history data
docker-compose run web bundle exec rake db:seed
# optionally download the external data before the first run,
# the data is downloaded in background but if you want to see something meaningful
# at the very first start you can download the data in advance
docker-compose run web bundle exec rake download:data
docker-compose up
```

Finally, do not forget to check the [Docker Compose documentation](
https://docs.docker.com/compose/overview/) for further details.
