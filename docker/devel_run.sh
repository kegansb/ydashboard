#! /bin/bash

# make sure we are always at the app root
cd "${BASH_SOURCE%/*}/.."

COMPOSE_FILES="-f docker-compose.yml -f docker-compose.development.yml"

# start the project
docker-compose $COMPOSE_FILES up --build
