
events {
    worker_connections 1024;
    use epoll;
}

http {
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  upstream ydashboard {
    server app:3000;
  }
   
  server {
    listen 80;

    # STDOUT/ERR is collected by Docker
    access_log /dev/stdout;
    error_log /dev/stderr debug;

    root /ydashboard/www;

    location ~ ^/assets/ {
      # use the precompiled *.gz files
      gzip_static on;
      expires 1y;
      add_header Cache-Control public;
      add_header ETag "";
    }

    location ~ ^/fonts/ {
      add_header Cache-Control public;
      add_header ETag "";
    }

    location / {
      # check for the maintenance mode
      if (-f $document_root/maintenance.html) {
        return 503;
      }

      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_redirect off;

      # static file
      if (-f $request_filename) {
          break;
      }

      # pass to the Rails server
      proxy_pass http://ydashboard;
    }
   
    error_page 503 /maintenance.html;
    location = /maintenance.html {
    }
  }
}
