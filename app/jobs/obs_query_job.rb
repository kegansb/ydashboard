require "ydashboard/obs_status_query"
require "ydashboard/obs_result_importer"
require "ydashboard/bugzilla_credentials"

class ObsQueryJob < ApplicationJob
  queue_as :default

  OBS_PROJECTS = [ "YaST:Head", "YaST:openSUSE:42.2", "YaST:openSUSE:42.3",
    "YaST:SLE-12:SP2", "YaST:SLE-12:SP3" ].freeze

  IBS_PROJECTS = [ "Devel:YaST:Head", "Devel:YaST:CASP:1.0", "Devel:YaST:SLE-12",
    "Devel:YaST:SLE-12-SP1", "Devel:YaST:SLE-12-SP2", "Devel:YaST:SLE-12-SP3" ]

  def perform(*args)
    Rails.logger.info "Starting #{self.class}"
    if Ydashboard::BugzillaCredentials.load.empty?
      Rails.logger.warn "Empty Bugzilla credentials, skipping OBS query"
      return
    end

    begin
      import_projects(OBS_PROJECTS, false)
      import_projects(IBS_PROJECTS, true)
    ensure
      # enqueue itself in 1 hour
      ObsQueryJob.set(wait: 1.hour).perform_later
    end
  end

private

  def import_projects(projects, internal_obs)
    projects.each do |obs_project|
      query = Ydashboard::ObsStatusQuery.new(obs_project, internal_obs)
      xml = query.run
      obs_importer = Ydashboard::ObsResultImporter.new(xml, internal_obs)
      obs_importer.save!
    end
  end

end
