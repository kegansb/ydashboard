class DashboardController < ApplicationController
  def index
    # FIXME: split this too long method
    @bugstat = BugStat.order(:date).last
    @bugstat_old = BugStat.where(date: @bugstat.date - 7).first if @bugstat
    @closed_bugstat = ClosedBugStat.order(:date).last
    @closed_bugstat_old = ClosedBugStat.where(date: @closed_bugstat.date - 7).first if @closed_bugstat
    @y2maintainers = Bug.where(assignee: "yast2-maintainers@suse.de").count
    
    head = ObsProject.find_by(name: "YaST:Head")
    if head
      @obs_head_failures = head.obs_repo_statuses.map{|a| a.failures}.sum
      @obs_head_factory_failures = head.obs_repo_statuses.find_by(name: "openSUSE_Factory").failures
      @obs_head_tw_failures = head.obs_repo_statuses.find_by(name: "openSUSE_Tumbleweed").failures
    end
    
    ihead = ObsProject.find_by(name: "Devel:YaST:Head")
    @ibs_head_sle15_failures = ihead.obs_repo_statuses.find_by(name: "SUSE_SLE-15_GA").failures if ihead
    casp = ObsProject.find_by(name: "Devel:YaST:CASP:1.0")
    @ibs_casp1_failures = casp.obs_repo_statuses.find_by(name: "CASP_1.0").failures if casp
    sp2 = ObsProject.find_by(name: "Devel:YaST:SLE-12-SP2")
    @ibs_sp2_failures = sp2.obs_repo_statuses.find_by(name: "SLE_12_SP2").failures if sp2

    if JenkinsStatus.where(internal: false).exists?
      @pjenkins_y2_failures = JenkinsStatus.where(internal: false, success: false)
        .select{|s| s.name.start_with?("yast-") && s.name.end_with?("-master") }.count
      @pjenkins_yui_failures = JenkinsStatus.where(internal: false, success: false)
        .select{|s| s.name.start_with?("libyui-") && s.name.end_with?("-master") }.count
      @pjenkins_yuistg_failures = JenkinsStatus.where(internal: false, success: false)
        .select{|s| s.name.start_with?("libyui-") && s.name.end_with?("-staging") }.count
    end

    if JenkinsStatus.where(internal: true).exists?
      @ijenkins_y2_failures = JenkinsStatus.where(internal: true, success: false)
        .select{|s| s.name.start_with?("yast-") && s.name.end_with?("-master") }.count
      @ijenkins_y2c_failures = JenkinsStatus.where(internal: true, success: false)
        .select{|s| s.name.start_with?("yast-") && s.name.end_with?("-CASP") }.count
      @ijenkins_yui_failures = JenkinsStatus.where(internal: true, success: false)
        .select{|s| s.name.start_with?("libyui-") && s.name.end_with?("-SLE-12") }.count
    end
      
    gh_org = GhOrg.find_by(name: "yast")

    if gh_org
      @gh_yast_pulls = 0
      @gh_yast_issues = 0
      # FIXME: this triggers a lot of SQL queries, optimize it
      gh_org.gh_repos.each do |repo|
        @gh_yast_pulls += repo.gh_issues.where(is_pull: true).count
        @gh_yast_issues += repo.gh_issues.where(is_pull: false).count
      end
    end

    gh_org = GhOrg.find_by(name: "libyui")
    if gh_org
      @gh_libyui_pulls = 0
      @gh_libyui_issues = 0
      # FIXME: this triggers a lot of SQL queries, optimize it
      gh_org.gh_repos.each do |repo|
        @gh_libyui_pulls += repo.gh_issues.where(is_pull: true).count
        @gh_libyui_issues += repo.gh_issues.where(is_pull: false).count
      end
    end
  end
end
