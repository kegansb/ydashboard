class BugStat < ApplicationRecord

  def total_open
    p0 + p1 + p2 + p3 + p4 + p5
  end

  def self.collect_for_today
    # update the statistics for toda or create a new one if it does not exist yet
    stat = BugStat.find_or_create_by(date: Date.current)

    stat.p0 = Bug.where(priority: 0).size
    stat.p1 = Bug.where(priority: 1).size
    stat.p2 = Bug.where(priority: 2).size
    stat.p3 = Bug.where(priority: 3).size
    stat.p4 = Bug.where(priority: 4).size
    stat.p5 = Bug.where(priority: 5).size

    stat.fresh_new = Bug.where(status: "NEW").size
    stat.confirmed = Bug.where(status: "CONFIRMED").size
    stat.in_progress = Bug.where(status: "IN_PROGRESS").size
    stat.reopened = Bug.where(status: "REOPENED").size

    stat.date = Date.current

    stat
  end
end
