// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require jquery.dataTables.min
//= require dataTables.bootstrap.min
//= require echarts.min
//= require gentelella-custom
//= require_tree .

// global config for echart graphs
var echart_theme = {
  color: [
      '#26B99A', '#34495E', '#e6b800', '#3498DB',
      '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
  ]
};
